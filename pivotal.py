import pandas as pd
import numpy as np

def print_data(data, message=''):
    print('\n'+'='*50+'\n')
    if message:
        print(message)
    print(data)

df = pd.read_excel('data.xlsx')
print_data(df)

pivotal_table = pd.pivot_table(df, index=['Rep',], values=['Price', ], aggfunc=[np.sum,])
print_data(pivotal_table, ' Pivotal with one row level')

pivotal_table = pd.pivot_table(df, index=['Rep', 'Name'], values=['Price', ], aggfunc=[np.sum,])
print_data(pivotal_table, '  Pivotal with two rows level')

pivotal_table = pd.pivot_table(df, index=['Rep', 'Name'], columns=['Product'], values=['Price', ], aggfunc=[np.sum,])
print_data(pivotal_table, 'Pivotal with two rows level, using the column "Product" as the pivotal columns')

pivotal_table = pd.pivot_table(
    df, index=['Rep', 'Name'], columns=['Product'], values=['Price', ], aggfunc=[np.sum,], fill_value=0
    )
print_data(
    pivotal_table, 
    '''Pivotal with two rows level, using the column "Product" as 
    the pivotal columns and filling with zero where teh value is NaN
    '''
    )


''' Create the xls with the pivotal table '''

''' Add a new sheet with the pivotal table to the data.xlsx file '''
writer = pd.ExcelWriter('data.xlsx', mode='a')# mode a is for adding
pivotal_table.to_excel(writer, 'Pivotal')
writer.save()

''' Create a new file with the pivotal table '''
writer = pd.ExcelWriter('data_pivotal.xlsx', mode='a')# mode a is for adding
pivotal_table.to_excel(writer, 'Pivotal')
writer.save()

''' Create a new file with the pivotal table with multples sheets, a sheep for each Rep '''
writer = pd.ExcelWriter('data_pivotal_multi_sheets.xlsx')
for manager in pivotal_table.index.get_level_values(0).unique():
    temp_df = pivotal_table.xs(manager, level=0)
    temp_df.to_excel(writer, manager)
writer.save()
